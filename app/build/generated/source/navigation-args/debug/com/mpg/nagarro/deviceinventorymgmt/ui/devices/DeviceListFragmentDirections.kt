package com.mpg.nagarro.deviceinventorymgmt.ui.devices

import android.os.Bundle
import androidx.navigation.NavDirections
import com.wadas.inventory.R
import kotlin.Int

public class DeviceListFragmentDirections private constructor() {
  private data class ActionDeviceListFragmentToAddEditDeviceFragment(
    public val deviceId: Int = 0
  ) : NavDirections {
    public override fun getActionId(): Int =
        R.id.action_device_list_fragment_to_addEditDeviceFragment

    public override fun getArguments(): Bundle {
      val result = Bundle()
      result.putInt("deviceId", this.deviceId)
      return result
    }
  }

  public companion object {
    public fun actionDeviceListFragmentToAddEditDeviceFragment(deviceId: Int = 0): NavDirections =
        ActionDeviceListFragmentToAddEditDeviceFragment(deviceId)
  }
}
