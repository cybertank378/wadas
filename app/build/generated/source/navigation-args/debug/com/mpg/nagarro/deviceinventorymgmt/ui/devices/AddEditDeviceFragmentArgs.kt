package com.mpg.nagarro.deviceinventorymgmt.ui.devices

import android.os.Bundle
import androidx.navigation.NavArgs
import kotlin.Int
import kotlin.jvm.JvmStatic

public data class AddEditDeviceFragmentArgs(
  public val deviceId: Int = 0
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("deviceId", this.deviceId)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): AddEditDeviceFragmentArgs {
      bundle.setClassLoader(AddEditDeviceFragmentArgs::class.java.classLoader)
      val __deviceId : Int
      if (bundle.containsKey("deviceId")) {
        __deviceId = bundle.getInt("deviceId")
      } else {
        __deviceId = 0
      }
      return AddEditDeviceFragmentArgs(__deviceId)
    }
  }
}
