package com.mpg.nagarro.deviceinventorymgmt.ui.employee

import android.os.Bundle
import androidx.navigation.NavArgs
import kotlin.Int
import kotlin.jvm.JvmStatic

public data class AddEditEmployeeFragmentArgs(
  public val employeeId: Int = 0
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("employeeId", this.employeeId)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): AddEditEmployeeFragmentArgs {
      bundle.setClassLoader(AddEditEmployeeFragmentArgs::class.java.classLoader)
      val __employeeId : Int
      if (bundle.containsKey("employeeId")) {
        __employeeId = bundle.getInt("employeeId")
      } else {
        __employeeId = 0
      }
      return AddEditEmployeeFragmentArgs(__employeeId)
    }
  }
}
