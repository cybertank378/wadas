package com.mpg.nagarro.deviceinventorymgmt.ui.employee

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.wadas.inventory.R

public class AddEditEmployeeFragmentDirections private constructor() {
  public companion object {
    public fun actionAddEditEmployeeFragmentToEmployeeListFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_addEditEmployeeFragment_to_employee_list_fragment)
  }
}
