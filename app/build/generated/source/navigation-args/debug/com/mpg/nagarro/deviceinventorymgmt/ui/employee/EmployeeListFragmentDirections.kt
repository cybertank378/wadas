package com.mpg.nagarro.deviceinventorymgmt.ui.employee

import android.os.Bundle
import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.wadas.inventory.R
import kotlin.Int

public class EmployeeListFragmentDirections private constructor() {
  private data class ActionEmployeeListFragmentToAddEditEmployeeFragment(
    public val employeeId: Int = 0
  ) : NavDirections {
    public override fun getActionId(): Int =
        R.id.action_employee_list_fragment_to_addEditEmployeeFragment

    public override fun getArguments(): Bundle {
      val result = Bundle()
      result.putInt("employeeId", this.employeeId)
      return result
    }
  }

  public companion object {
    public fun actionEmployeeListFragmentToDeviceListFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_employeeListFragment_to_device_list_fragment)

    public fun actionEmployeeListFragmentToAddEditEmployeeFragment(employeeId: Int = 0):
        NavDirections = ActionEmployeeListFragmentToAddEditEmployeeFragment(employeeId)
  }
}
