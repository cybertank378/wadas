package com.mpg.nagarro.deviceinventorymgmt.ui.devices

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.wadas.inventory.R

public class AddEditDeviceFragmentDirections private constructor() {
  public companion object {
    public fun actionAddEditDeviceFragmentToDeviceListFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_add_edit_device_fragment_to_device_list_fragment)
  }
}
