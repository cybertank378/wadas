package com.mpg.nagarro.deviceinventorymgmt.ui.inventory

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.wadas.inventory.R

public class DeviceAllotmentFragmentDirections private constructor() {
  public companion object {
    public fun actionDeviceAllotmentFragmentToDeviceAllottedListFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_deviceAllotmentFragment_to_deviceAllottedListFragment)
  }
}
