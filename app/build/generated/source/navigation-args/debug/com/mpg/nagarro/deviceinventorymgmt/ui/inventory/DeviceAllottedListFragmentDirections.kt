package com.mpg.nagarro.deviceinventorymgmt.ui.inventory

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.wadas.inventory.R

public class DeviceAllottedListFragmentDirections private constructor() {
  public companion object {
    public fun actionDeviceAllottedListFragmentToDeviceAllotmentFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_deviceAllottedListFragment_to_deviceAllotmentFragment)

    public fun actionDeviceAllottedListFragmentToDeviceListFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_deviceAllottedListFragment_to_device_list_fragment)
  }
}
