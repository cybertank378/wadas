// Generated by data binding compiler. Do not edit!
package com.wadas.inventory.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mpg.nagarro.deviceinventorymgmt.ui.employee.EmployeeListViewModel;
import com.wadas.inventory.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class EmployeeListFragmentBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView employeeList;

  @NonNull
  public final FloatingActionButton floatingActionButton;

  @NonNull
  public final LinearLayout noInventoryLayout;

  @NonNull
  public final ImageView noItemsIcon;

  @NonNull
  public final TextView noItemsText;

  @Bindable
  protected EmployeeListViewModel mViewModel;

  protected EmployeeListFragmentBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RecyclerView employeeList, FloatingActionButton floatingActionButton,
      LinearLayout noInventoryLayout, ImageView noItemsIcon, TextView noItemsText) {
    super(_bindingComponent, _root, _localFieldCount);
    this.employeeList = employeeList;
    this.floatingActionButton = floatingActionButton;
    this.noInventoryLayout = noInventoryLayout;
    this.noItemsIcon = noItemsIcon;
    this.noItemsText = noItemsText;
  }

  public abstract void setViewModel(@Nullable EmployeeListViewModel viewModel);

  @Nullable
  public EmployeeListViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static EmployeeListFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.employee_list_fragment, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static EmployeeListFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<EmployeeListFragmentBinding>inflateInternal(inflater, R.layout.employee_list_fragment, root, attachToRoot, component);
  }

  @NonNull
  public static EmployeeListFragmentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.employee_list_fragment, null, false, component)
   */
  @NonNull
  @Deprecated
  public static EmployeeListFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<EmployeeListFragmentBinding>inflateInternal(inflater, R.layout.employee_list_fragment, null, false, component);
  }

  public static EmployeeListFragmentBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static EmployeeListFragmentBinding bind(@NonNull View view, @Nullable Object component) {
    return (EmployeeListFragmentBinding)bind(component, view, R.layout.employee_list_fragment);
  }
}
