// Generated by data binding compiler. Do not edit!
package com.wadas.inventory.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.mpg.nagarro.deviceinventorymgmt.data.entity.DeviceInventory;
import com.mpg.nagarro.deviceinventorymgmt.ui.inventory.DeviceAllottedListViewModel;
import com.wadas.inventory.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class InventoryListItemBinding extends ViewDataBinding {
  @NonNull
  public final CheckBox cbStatusLost;

  @NonNull
  public final CheckBox cbStatusReturned;

  @NonNull
  public final ImageView ivDeleteRow;

  @NonNull
  public final TextView tvDeviceName;

  @NonNull
  public final TextView tvDeviceReturnedDate;

  @NonNull
  public final TextView tvDeviceStatus;

  @NonNull
  public final TextView tvEmployeeName;

  @Bindable
  protected DeviceAllottedListViewModel mViewModel;

  @Bindable
  protected DeviceInventory mDeviceinfo;

  protected InventoryListItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      CheckBox cbStatusLost, CheckBox cbStatusReturned, ImageView ivDeleteRow,
      TextView tvDeviceName, TextView tvDeviceReturnedDate, TextView tvDeviceStatus,
      TextView tvEmployeeName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cbStatusLost = cbStatusLost;
    this.cbStatusReturned = cbStatusReturned;
    this.ivDeleteRow = ivDeleteRow;
    this.tvDeviceName = tvDeviceName;
    this.tvDeviceReturnedDate = tvDeviceReturnedDate;
    this.tvDeviceStatus = tvDeviceStatus;
    this.tvEmployeeName = tvEmployeeName;
  }

  public abstract void setViewModel(@Nullable DeviceAllottedListViewModel viewModel);

  @Nullable
  public DeviceAllottedListViewModel getViewModel() {
    return mViewModel;
  }

  public abstract void setDeviceinfo(@Nullable DeviceInventory deviceinfo);

  @Nullable
  public DeviceInventory getDeviceinfo() {
    return mDeviceinfo;
  }

  @NonNull
  public static InventoryListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.inventory_list_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static InventoryListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<InventoryListItemBinding>inflateInternal(inflater, R.layout.inventory_list_item, root, attachToRoot, component);
  }

  @NonNull
  public static InventoryListItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.inventory_list_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static InventoryListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<InventoryListItemBinding>inflateInternal(inflater, R.layout.inventory_list_item, null, false, component);
  }

  public static InventoryListItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static InventoryListItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (InventoryListItemBinding)bind(component, view, R.layout.inventory_list_item);
  }
}
