package com.wadas.inventory.common

import androidx.lifecycle.ViewModel


/**
 * Shared viewModel class. This viewModel is in activity scope.
 * So we can use this to share data between fragments.
 */
class SharedViewModel : ViewModel() {

}