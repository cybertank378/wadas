package com.wadas.inventory.util

interface ExporterListener {
    fun success(s: String)

    fun fail(message: String,exception:String)
}