package com.wadas.inventory.di

import com.wadas.inventory.data.InventoryDataSource
import com.wadas.inventory.data.Repository
import com.wadas.inventory.data.RepositoryImpl
import com.wadas.inventory.data.local.InventoryDao
import com.wadas.inventory.data.local.LocalInventoryRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {
//    @Binds
//    abstract fun getRepository(inventoryDataSource: RepositoryImpl): Repository
//
//    @Binds
//    abstract fun getLocalRepository(inventoryDao: LocalInventoryRepository): InventoryDataSource

    @Provides
    internal fun getRepository(inventoryDataSource: InventoryDataSource): Repository {
        return RepositoryImpl(inventoryDataSource)
    }
    @Provides
    internal fun getLocalRepository(inventoryDao: InventoryDao): InventoryDataSource {
        return LocalInventoryRepository(inventoryDao)
    }

}