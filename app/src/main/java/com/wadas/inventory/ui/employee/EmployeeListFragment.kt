package com.wadas.inventory.ui.employee

import android.view.View
import androidx.navigation.fragment.findNavController
import com.wadas.inventory.BR
import com.wadas.inventory.R
import com.wadas.inventory.base.BaseFragment
import com.wadas.inventory.data.entity.EmployeeEntity
import com.wadas.inventory.databinding.EmployeeListFragmentBinding
import com.wadas.inventory.ui.employee.adapter.EmployeeListAdapter
import com.wadas.inventory.util.showSnackbar
import javax.inject.Inject

class EmployeeListFragment : BaseFragment<EmployeeListFragmentBinding, EmployeeListViewModel>(),
    EmployeeListAdapter.OnItemClickListener {


    override val bindingVariable: Int
        get() = BR.viewModel

    @Inject
    lateinit var listAdapter: EmployeeListAdapter

    override fun getLayout() = R.layout.employee_list_fragment

    override fun getViewModel() = EmployeeListViewModel::class.java

    override fun onCreateView(rootView: View) {

        setUpAdapter()

        viewDataBinding.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.addEditEmployeeFragment)
        }

        viewModel.showMessage.observe(viewLifecycleOwner, {
            viewDataBinding.root.showSnackbar(it)
        })

    }

    /**Add Adapter to view*/
    private fun setUpAdapter() {
        listAdapter.setItemClickListener(this)
        viewDataBinding.employeeList.adapter = listAdapter
    }

    override fun onItemClicked(position: Int, item: EmployeeEntity) {
        viewModel.deleteRow(item)
    }

}