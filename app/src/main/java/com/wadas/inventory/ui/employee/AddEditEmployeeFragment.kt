package com.wadas.inventory.ui.employee

import android.view.View
import androidx.navigation.fragment.findNavController
import com.wadas.inventory.BR
import com.wadas.inventory.R
import com.wadas.inventory.base.BaseFragment
import com.wadas.inventory.databinding.AddEditEmployeeFragmentBinding

class AddEditEmployeeFragment :
    BaseFragment<AddEditEmployeeFragmentBinding, AddEditEmployeeViewModel>() {

    override val bindingVariable: Int
        get() = BR.viewModel

    override fun getLayout() = R.layout.add_edit_employee_fragment

    override fun getViewModel() = AddEditEmployeeViewModel::class.java

    override fun onCreateView(rootView: View) {

        viewDataBinding.btnAddEmployee.setOnClickListener {
            viewModel.addEmployee()
            findNavController().popBackStack()
        }

    }
}