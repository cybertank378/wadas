package com.wadas.inventory.ui.devices

import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.wadas.inventory.BR
import com.wadas.inventory.R
import com.wadas.inventory.base.BaseFragment
import com.wadas.inventory.data.entity.DeviceEntity
import com.wadas.inventory.databinding.DeviceListFragmentBinding
import com.wadas.inventory.ui.devices.adapter.DeviceListAdapter
import com.wadas.inventory.ui.devices.adapter.OnDeviceItemClickListener
import com.wadas.inventory.util.DialogCallBack
import com.wadas.inventory.util.EventObserver
import com.wadas.inventory.util.Utils.showDialog
import com.wadas.inventory.util.showSnackbar
import com.wadas.inventory.util.showToast
import timber.log.Timber
import javax.inject.Inject

class DeviceListFragment : BaseFragment<DeviceListFragmentBinding, DeviceListViewModel>(),
    OnDeviceItemClickListener, DialogCallBack<DeviceEntity> {

    override val bindingVariable: Int
        get() = BR.viewModel

    @Inject
    lateinit var listAdapter: DeviceListAdapter

    override fun getLayout() = R.layout.device_list_fragment

    override fun getViewModel() = DeviceListViewModel::class.java

    override fun onCreateView(rootView: View) {
        setUpAdapter()

        viewDataBinding.floatingActionButton.setOnClickListener {
            navigateToAddNewDevice()
        }

        listAdapter.setItemClickListener(this)

        viewModel.showMessage.observe(viewLifecycleOwner, {
            viewDataBinding.root.showSnackbar(it)

        })
        viewModel.isItemClicked.observe(viewLifecycleOwner, Observer {
            Timber.e("Item Clicked--------")
            viewDataBinding.root.showSnackbar("Item clocked------")
        })
    }

    private fun navigateToAddNewDevice() {
        val action = DeviceListFragmentDirections
            .actionDeviceListFragmentToAddEditDeviceFragment(
                0
            )
        findNavController().navigate(action)
    }

    private fun openDeviceDetails(deviceId: Int) {
        val action =
            DeviceListFragmentDirections.actionDeviceListFragmentToAddEditDeviceFragment(deviceId)
        findNavController().navigate(action)
    }

    private fun setUpAdapter() {
        viewDataBinding.deviceList.adapter = listAdapter
    }

    override fun onItemClicked(position: Int, item: DeviceEntity) {
        showDialog("Device : ${item.name}", getString(R.string.alert_message), this, item)
    }

    override fun onPositiveButtonClicked(data: DeviceEntity) {
        viewModel.deleteRowAction(data)
    }

}