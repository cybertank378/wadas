package com.wadas.inventory.ui.employee

import androidx.lifecycle.viewModelScope
import com.wadas.inventory.base.BaseViewModel
import com.wadas.inventory.common.SingleLiveEvent
import com.wadas.inventory.data.Repository
import com.wadas.inventory.data.entity.EmployeeEntity
import com.wadas.inventory.util.Utils
import kotlinx.coroutines.launch
import javax.inject.Inject

class AddEditEmployeeViewModel @Inject constructor(private val repository: Repository) :
    BaseViewModel() {
    val empEmail = SingleLiveEvent<String>()
    val empName = SingleLiveEvent<String>()

    fun addEmployee() {
        val employeeName = empName.value
        val employeeEmail = empEmail.value

        if (Utils.isNullOrEmpty(employeeName) || Utils.isNullOrEmpty(employeeEmail))
            return

        if (Utils.isValidEmail(employeeEmail!!))
            viewModelScope.launch {
                repository.addEmployee(EmployeeEntity(empName.value!!, employeeEmail))
            }
    }
}